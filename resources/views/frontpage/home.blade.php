<!DOCTYPE html>
<html lang="en-US" dir="ltr">

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!-- ===============================================-->
    <!--    Document Title-->
    <!-- ===============================================-->
    <title>CarDealer</title>


    <!-- ===============================================-->
    <!--    Favicons-->
    <!-- ===============================================-->
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('frontpage/assets/img/favicons/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('frontpage/assets/img/favicons/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('frontpage/assets/img/favicons/favicon-16x16.png')}}">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('frontpage/assets/img/favicons/favicon.ico')}}">
    <link rel="manifest" href="{{asset('frontpage/assets/img/favicons/manifest.json')}}">
    <meta name="msapplication-TileImage" content="{{asset('frontpage/assets/img/favicons/mstile-150x150.png')}}">
    <meta name="theme-color" content="#ffffff">


    <!-- ===============================================-->
    <!--    Stylesheets-->
    <!-- ===============================================-->
    <link href="{{asset('frontpage/assets/css/theme.css')}}" rel="stylesheet" />

    {{-- Icon --}}
    <link rel="stylesheet" href="{{asset('template/assets/vendor/fonts/boxicons.css')}}" />

  </head>


  <body>
@php
if(Auth::check()) {
    $u = Auth::user();
    $role = "User";
    if($u->is_admin == 2) {
        $role = "Admin";
    }
}
@endphp

    <!-- ===============================================-->
    <!--    Main Content-->
    <!-- ===============================================-->
    <main class="main" id="top">
      <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top" data-navbar-on-scroll="data-navbar-on-scroll">
        <div class="container"><a class="navbar-brand d-inline-flex" href="index.html"><span class="text-1000 fs-3 fw-bold ms-2 text-gradient">CarDealer</span></a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"> </span></button>
          <div class="collapse navbar-collapse border-top border-lg-0 my-2 mt-lg-0 justify-content-end" id="navbarSupportedContent">
            
            {{-- <form class="d-flex mt-4 mt-lg-0 ms-lg-auto ms-xl-0">
              <button class="btn btn-white shadow-warning text-warning" type="submit"> --}}
                <ul class="d-inline-flex list-unstyled mt-3 align-items-center">
                  @if(Auth::check() && ( Auth::User()->role_id == 1))
                  <li>
                    <a href="/pelanggan" class="nav-link">Pelanggan</a>
                  </li>
                  @endif
                  @if(Auth::check() && ( Auth::User()->role_id == 2))
                  <li>
                    <a href="/admin" class="nav-link">Admin</a>
                  </li>
                  @endif
                  <li>
                    @auth  
                
                    <a class="nav-links bg-primary pt-1 pb-2 px-3 border rounded mt-2 " href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                                            <i class="bx bx-log-out me-2"></i>
                                            <span class="align-middle">{{ __('Logout') }}</span> 
                    </a>
                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                          @csrf
                      </form>
                  @endauth
                  @guest
                  
                  <a class="nav-links bg-primary pt-1 pb-2 px-3 border rounded mt-2 align-right" href="/login">
                    <i class="bx bx-log-in me-2"></i>
                    <span class="align-middle">Login</span>
                  </a>
                  @endguest
                  </li>
                </ul>
                
            {{-- </button>
            </form> --}}
          </div>
        </div>
      </nav>
      <section class="py-5 overflow-hidden bg-primary" id="home">
        <div class="container">
          <div class="row flex-center">
            <div class="col-md-5 col-lg-6 order-0 order-md-1 pb-7 ps-10"><a class="img-landing-banner" href="#!"><img class="img-fluid" src="{{asset('frontpage/assets/img/gallery/hero-img.png')}}" alt="hero-header" /></a></div>
            <div class="col-md-7 col-lg-6 pt-9 pb-5 text-md-start text-center">
              <h1 class="display-1 fs-md-5 fs-lg-6 fs-xl-8 text-light">Butuh Kendaraan?</h1>
              <h1 class="text-800 mb-5 fs-4">CarDealer hadir untuk memudahkan <br class="d-none d-xxl-block" />anda mencari mobil yang tepat!</h1>
            </div>
          </div>
        </div>
      </section>

      <!-- ============================================-->
      <!-- <section> begin ============================-->
      <section class="py-0">

        <div class="container">
          <div class="row h-100 gx-2 mt-7">
            @forelse ($mobil as $item)
            @if ($item->status_id == 1)
            <div class="col-sm-6 col-md-4 col-xl mb-5 h-100">
              <div class="card card-span h-100 rounded-3"><img class="img-fluid rounded-3 h-100" src="{{asset('images/' . $item->img)}}" alt="..."/>
                <div class="card-body ps-0">
                  <h5 class="fw-bold text-1000 text-truncate mb-1">{{$item->nama}}</h5>
                  <div><span class="text-warning me-2"><i class="fas fa-map-marker-alt"></i></span><span class="text-primary">{{$item->desc}}</span>
                  </div>
                  <span class="text-1000 fw-bold">Rp. {{$item->harga}}</span>
                </div>
              </div>
              <div class="d-grid gap-2"><a class="btn btn-lg btn-danger" href="/mobil/{{$item->id}}" role="button">Order now</a></div>
            </div>
            @endif
            @empty
                <h1 class="text-center">Tidak Ada Data Mobil</h1>
            @endforelse
          </div>
        </div><!-- end of .container-->

      </section>
      <!-- <section> close ============================-->
      <!-- ============================================-->




      <!-- ============================================-->
      <!-- <section> begin ============================-->
  
      <!-- <section> close ============================-->
      <!-- ============================================-->




      <!-- ============================================-->
      <!-- <section> begin ============================-->
     
      <!-- <section> close ============================-->
      <!-- ============================================-->


      <!-- ============================================-->
      <!-- <section> begin ============================-->
      
      <!-- <section> close ============================-->
      <!-- ============================================-->


      <!-- ============================================-->
      <!-- <section> begin ============================-->

      <!-- <section> close ============================-->
      <!-- ============================================-->




      <!-- ============================================-->
      <!-- <section> begin ============================-->
    
      <!-- <section> close ============================-->
      <!-- ============================================-->




      <!-- ============================================-->
      <!-- <section> begin ============================-->
    
      <!-- <section> close ============================-->
      <!-- ============================================-->


    

      <!-- ============================================-->
      <!-- <section> begin ============================-->
      <section class="py-0 pt-5 bg-1000">

        <div class="container">
          <div class="row flex-center pb-3">
            <div class="col-md-6 order-0">
              <p class="text-200 text-center text-md-start">All rights Reserved &copy; Team 9 Batch 33 Sanbercode, 2022</p>
            </div>
            <div class="col-md-6 order-1">
              <p class="text-200 text-center text-md-end"> Made with&nbsp;
                <svg class="bi bi-suit-heart-fill" xmlns="http://www.w3.org/2000/svg" width="15" height="15" fill="#FFB30E" viewBox="0 0 16 16">
                  <path d="M4 1c2.21 0 4 1.755 4 3.92C8 2.755 9.79 1 12 1s4 1.755 4 3.92c0 3.263-3.234 4.414-7.608 9.608a.513.513 0 0 1-.784 0C3.234 9.334 0 8.183 0 4.92 0 2.755 1.79 1 4 1z"></path>
                </svg>&nbsp;by&nbsp;<a class="text-200 fw-bold" href="https://themewagon.com/" target="_blank">ThemeWagon </a>
              </p>
            </div>
          </div>
        </div><!-- end of .container-->

      </section>
      <!-- <section> close ============================-->
      <!-- ============================================-->


    </main>
    <!-- ===============================================-->
    <!--    End of Main Content-->
    <!-- ===============================================-->




    <!-- ===============================================-->
    <!--    JavaScripts-->
    <!-- ===============================================-->
    <script src="{{asset('frontpage/vendors/@popperjs/popper.min.js')}}"></script>
    <script src="{{asset('frontpage/vendors/bootstrap/bootstrap.min.js')}}"></script>
    <script src="{{asset('frontpage/vendors/is/is.min.js')}}"></script>
    <script src="{{asset('frontpage/https://polyfill.io/v3/polyfill.min.js?features=window.scroll')}}"></script>
    <script src="{{asset('frontpage/vendors/fontawesome/all.min.js')}}"></script>
    <script src="{{asset('frontpage/assets/js/theme.js')}}"></script>

    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@200;300;400;600;700;900&amp;display=swap" rel="stylesheet">
  </body>

</html>