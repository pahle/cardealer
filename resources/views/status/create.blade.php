@extends('layouts.master')
@section('judul')
    Halaman Tambah Status Mobil
@endsection

@section('content')
<form action="/status" method="POST" enctype="multipart/form-data">
    @csrf

    <div class="mb-3">
      <label class="form-label">Status Mobil</label>
      <input type="text" name="status_name" class="form-control"/>
    </div>

    @error('nama')
        <div class="alert alert-primary" role="alert">{{$message}}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Tambah</button>
  </form>
@endsection