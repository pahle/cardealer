@extends('layouts.master')
@section('judul')
    Halaman Edit Status Mobil
@endsection

@section('content')
<form action="/status/{{$status->id}}" method="POST">
    @csrf
    @method('put')
    <div class="mb-3">
      <label class="form-label">Status Mobil</label>
      <input type="text" name="status_name" class="form-control" value="{{$status->status_name}}"/>
    </div>

    @error('nama')
        <div class="alert alert-primary" role="alert">{{$message}}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection