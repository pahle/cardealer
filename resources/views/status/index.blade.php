@extends('layouts.master')
@section('judul')
    Halaman List Status Mobil
@endsection

@push('script')
    
<script src="{{asset('template/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('template/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#tabelStatus").DataTable();
  });
</script>

@endpush --}}

@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.5/datatables.min.css"/>
@endpush

@section('content')
<a href="/status/create" class="btn btn-primary my-3">Tambah</a>
<div class="card p-3">
    <h5 class="card-header">Table Role</h5>
    <div class="table-responsive text-nowrap">
      <table class="table" id="tabelStatus">
        <thead>
          <tr>
            <th>Id</th>
            <th>Status Mobil</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody class="table-border-bottom-0">
          @forelse ($status as $item)
          <tr>
            <td><i class="fab fa-angular fa-lg text-danger me-3"></i> <strong>{{$item->id}}</strong></td>
            <td>{{$item->status_name}}</td>
            <td>
              <div class="dropdown">
                <button type="button" class="btn p-0 dropdown-toggle hide-arrow" data-bs-toggle="dropdown">
                  <i class="bx bx-dots-vertical-rounded"></i>
                </button>
                <div class="dropdown-menu">
                  <form action="/status/{{$item->id}}" method="POST">
                    @csrf
                    @method('delete')
                    <a class="dropdown-item" href="/status/{{$item->id}}/edit"
                      ><i class="bx bx-edit-alt me-1"></i> Edit</a
                    >
                    <button class="dropdown-item" type="submit"
                      ><i class="bx bx-trash me-1"></i> Delete</button
                    >
                  </form>
                </div>
              </div>
            </td>
          </tr>
          @empty
              
          @endforelse
        </tbody>
      </table>
</div>
@endsection