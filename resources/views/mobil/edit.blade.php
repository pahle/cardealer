@extends('layouts.master')
@section('judul')
    Halaman Edit Mobil
@endsection

@section('content')
<form action="/mobil/{{$mobil->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('put')
    <div class="mb-3">
      <label class="form-label">Nama Mobil</label>
      <input type="text" name="nama" class="form-control" value="{{$mobil->nama}}"/>
    </div>

    @error('nama')
        <div class="alert alert-primary" role="alert">{{$message}}</div>
    @enderror

    <div class="mb-3">
        <label class="form-label">Deskripsi</label>
        <textarea name="desc" class="form-control" rows="3">{{$mobil->desc}}</textarea>
    </div>

    @error('desc')
        <div class="alert alert-primary" role="alert">{{$message}}</div>
    @enderror

    <div class="mb-3">
        <label class="form-label">Harga</label>
        <input type="number" name="harga" class="form-control" value="{{$mobil->harga}}"/>
    </div>

    @error('harga')
        <div class="alert alert-primary" role="alert">{{$message}}</div>
    @enderror

    <div class="mb-3">
        <label class="form-label">Gambar</label>
        <input type="file" name="img" class="form-control"/>
    </div>

    @error('img')
        <div class="alert alert-primary" role="alert">{{$message}}</div>
    @enderror

    <div class="mb-3">
        <label class="form-label">Example select</label>
        <select name="status_id" class="form-select">
         <option selected>---Pilih Status Mobil---</option>
            @foreach ($status as $item)
            @if ($item->id === $mobil->status_id) 
                <option value="{{$item->id}}" selected>{{$item->status_name}}</option>
            @else
                <option value="{{$item->id}}">{{$item->status_name}}</option>  
            @endif
            @endforeach
        </select>
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection