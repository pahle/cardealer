@extends('layouts.master')
@section('judul')
    Halaman Tambah Mobil
@endsection

@section('content')
<form action="/mobil" method="POST" enctype="multipart/form-data">
    @csrf

    <div class="mb-3">
      <label class="form-label">Nama Mobil</label>
      <input type="text" name="nama" class="form-control"/>
    </div>

    @error('nama')
        <div class="alert alert-primary" role="alert">{{$message}}</div>
    @enderror

    <div class="mb-3">
        <label class="form-label">Deskripsi</label>
        <textarea name="desc" class="form-control" rows="3"></textarea>
    </div>

    @error('desc')
        <div class="alert alert-primary" role="alert">{{$message}}</div>
    @enderror

    <div class="mb-3">
        <label class="form-label">Harga</label>
        <input type="number" name="harga" class="form-control"/>
    </div>

    @error('harga')
        <div class="alert alert-primary" role="alert">{{$message}}</div>
    @enderror

    <div class="mb-3">
        <label class="form-label">Gambar</label>
        <input type="file" name="img" class="form-control"/>
    </div>

    @error('img')
        <div class="alert alert-primary" role="alert">{{$message}}</div>
    @enderror

    <div class="mb-3">
        <label class="form-label">Example select</label>
        <select name="status_id" class="form-select">
         <option selected>---Pilih Status Mobil---</option>
            @foreach ($status as $item)
                <option value="{{$item->id}}">{{$item->status_name}}</option>
            @endforeach
        </select>
    </div>

    <button type="submit" class="btn btn-primary">Tambah</button>
  </form>
@endsection