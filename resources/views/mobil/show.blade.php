@extends('layouts.master')
@section('judul')
    Halaman Detail Mobil
@endsection
@section('content')

<a href="/" class="btn btn-primary btn-sm my-2">Kembali</a>

<div class="row">
    <div class="col-12">
        <div class="card" style="">
            <img src="{{asset('images/'.$mobil->img)}}" class="card-img-top" alt="...">
            <div class="card-body">
              <h3 class="card-title">{{$mobil->nama}}</h3>
              <p class="card-text">Desc : {{$mobil->desc}}</p>
              <p class="card-text fw-bolder">Status : {{$mobil->status->status_name}}</p>
              <h3 class="card-title">Tagihan</h3>
              <p class="card-text">Harga         :<strong> Rp. {{$mobil->harga}}</strong></p>
              <p class="card-text">Pajak 20%     :<strong> Rp. {{$mobil->harga * 0.2}}</strong></p>
              <p class="card-text">Biaya Lainnya :<strong> Rp. {{$mobil->harga * 0.4}}</strong></p>
                <hr>
              <p class="card-text">Total :<strong> Rp. {{$mobil->harga + ($mobil->harga * 0.2) + ($mobil->harga * 0.4)}}</strong></p>
              <form action="/order" method="POST">
                @csrf
                    <input type="hidden" value="{{$mobil->id}}" name="mobil_id">
                    <input type="hidden" value="{{$mobil->harga + ($mobil->harga * 0.2) + ($mobil->harga * 0.4)}}" name="tagihan">
                    <button type="submit" class="btn btn-warning">Beli</button>
              </form>
            </div>
        </div>
    </div>
</div>
@endsection