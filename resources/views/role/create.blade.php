@extends('layouts.master')
@section('judul')
    Halaman Tambah Role User
@endsection

@section('content')
<form action="/role" method="POST" enctype="multipart/form-data">
    @csrf

    <div class="mb-3">
      <label class="form-label">Nama Role</label>
      <input type="text" name="nama" class="form-control"/>
    </div>

    @error('nama')
        <div class="alert alert-primary" role="alert">{{$message}}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Tambah</button>
  </form>
@endsection