@extends('layouts.master')
@section('judul')
    Halaman Edit Role User
@endsection

@section('content')
<form action="/role/{{$role->id}}" method="POST">
    @csrf
    @method('put')
    <div class="mb-3">
      <label class="form-label">Nama Role</label>
      <input type="text" name="nama" class="form-control" value="{{$role->nama}}"/>
    </div>

    @error('nama')
        <div class="alert alert-primary" role="alert">{{$message}}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection