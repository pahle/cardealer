@extends('layouts.master')
@section('judul')
    Halaman Edit User
@endsection

@section('content')
<form action="/user/{{$user->id}}" method="POST">
    @csrf
    @method('put')
    <div class="mb-3">
      <label class="form-label">Nama User</label>
      <input type="text" name="name" class="form-control" value="{{$user->name}}"/>
    </div>

    @error('name')
        <div class="alert alert-primary" role="alert">{{$message}}</div>
    @enderror

    <div class="mb-3">
        <label class="form-label">Email</label>
        <textarea name="email" class="form-control" rows="3">{{$user->email}}</textarea>
    </div>

    @error('email')
        <div class="alert alert-primary" role="alert">{{$message}}</div>
    @enderror

    <div class="mb-3">
        <label class="form-label">Password</label>
        <input type="password" name="password" class="form-control" value="{{$user->password}}"/>
    </div>

    @error('password')
        <div class="alert alert-primary" role="alert">{{$message}}</div>
    @enderror

    <div class="mb-3">
        <label class="form-label">Role</label>
        <select name="status_id" class="form-select">
         <option selected>---Pilih Role---</option>
            @foreach ($role as $item)
            @if ($item->id === $user->role_id) 
                <option value="{{$item->id}}" selected>{{$item->nama}}</option>
            @else
                <option value="{{$item->id}}">{{$item->nama}}</option>  
            @endif
            @endforeach
        </select>
    </div>

    <button type="submit" class="btn btn-primary">Tambah</button>
  </form>
@endsection