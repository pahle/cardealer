@extends('layouts.master')
@section('judul')
    Halaman Edit Review
@endsection

@section('content')
<form action="/review/{{$review->id}}" method="POST">
    @csrf
    @method('put')
    <div class="mb-3">
        <label class="form-label">Review</label>
        <textarea name="review" class="form-control" rows="3" value="{{$review->review}}"></textarea>
    </div>

    @error('review')
        <div class="alert alert-primary" role="alert">{{$message}}</div>
    @enderror

    <div class="mb-3">
        <label class="form-label">Orderan Mobil</label>
        <select name="order_id" class="form-select">
         <option selected>---Pilih Orderan Mobil---</option>
            @foreach ($order as $item)
            @if ($item->id === $review->order_id) 
                <option value="{{$item->id}}" selected>{{$item->id}}</option>
            @else
                <option value="{{$item->id}}">{{$item->id}}</option>  
            @endif

            @endforeach
        </select>
    </div>

    <button type="submit" class="btn btn-primary">Tambah</button>
</form>
@endsection