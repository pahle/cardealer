@extends('layouts.master')
@section('judul')
    Halaman List Review
@endsection

@push('script')
    
<script src="{{asset('template/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('template/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#tabelMobil").DataTable();
  });
</script>

@endpush --}}

@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.5/datatables.min.css"/>
@endpush

@section('content')
<a href="/review/create" class="btn btn-primary my-3">Tambah</a>
<div class="card p-3">
    <h5 class="card-header">Table Basic</h5>
    <div class="table-responsive text-nowrap">
      <table class="table" id="tabelMobil">
        <thead>
          <tr>
            <th>Review</th>
            <th>Order</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody class="table-border-bottom-0">
          @forelse ($review as $item)
          <tr>
            <td><i class="fab fa-angular fa-lg text-danger me-3"></i> <strong>{{$item->review}}</strong></td>
            <td>{{$item->order->mobil->nama}}</td>
            <td>
              <div class="dropdown">
                <button type="button" class="btn p-0 dropdown-toggle hide-arrow" data-bs-toggle="dropdown">
                  <i class="bx bx-dots-vertical-rounded"></i>
                </button>
                <div class="dropdown-menu">
                  <form action="/review/{{$item->id}}" method="POST">
                    @csrf
                    @method('delete')
                    <a class="dropdown-item" href="/review/{{$item->id}}/edit"
                      ><i class="bx bx-edit-alt me-1"></i> Edit</a
                    >
                    <button class="dropdown-item" type="submit"
                      ><i class="bx bx-trash me-1"></i> Delete</button
                    >
                  </form>
                </div>
              </div>
            </td>
          </tr>
          @empty
              
          @endforelse
        </tbody>
      </table>
@endsection