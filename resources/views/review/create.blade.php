@extends('layouts.master')
@section('judul')
    Halaman Review
@endsection

@section('content')
<form action="/review" method="POST">
    @csrf
    <div class="mb-3">
        <label class="form-label">Review</label>
        <textarea name="review" class="form-control" rows="3"></textarea>
    </div>

    @error('review')
        <div class="alert alert-primary" role="alert">{{$message}}</div>
    @enderror

    <div class="mb-3">
        <label class="form-label">Orderan Mobil</label>
        <select name="order_id" class="form-select">
         <option selected>---Pilih Orderan Mobil---</option>
            @foreach ($order as $item)
                <option value="{{$item->id}}">{{$item->mobil->nama}}</option>
            @endforeach
        </select>
    </div>

    <button type="submit" class="btn btn-primary">Tambah</button>
</form>
@endsection