<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('status')->insert([
            [
                'status_name' => 'Belum Terjual',
            ],
            [
                'status_name' => 'Terjual',
            ]
        ]);

        DB::table('role')->insert([
            [
                'nama' => 'User',
            ],
            [
                'nama' => 'Admin',
            ]
        ]);

        DB::table('users')->insert([
            [
                'name' => 'Admin',
                'email' => 'demo@admin.com',
                'password' => '$2y$10$1fBzIAiHM6SC2cqdWeYn6e2l4M8pNq6ToAAfAWAdHQSPxwKCLEISC',
                'role_id' => '2',
            ],
            [
                'name' => 'User',
                'email' => 'user@admin.com',
                'password' => '$2y$10$1fBzIAiHM6SC2cqdWeYn6e2l4M8pNq6ToAAfAWAdHQSPxwKCLEISC',
                'role_id' => '1',
            ]
        ]);
    }
}