<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mobil extends Model
{
    protected $table = 'mobil';

    protected $fillable = ['nama', 'desc', 'harga', 'img', 'status_id'];

    public function status()
    {
        return $this->belongsTo('App\Status', 'status_id');
    }

    public function order()
    {
        return $this->hasMany('App\Order');
    }
}