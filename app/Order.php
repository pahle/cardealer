<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'order';

    protected $fillable = ['tagihan', 'mobil_id', 'users_id'];

    public function mobil()
    {
        return $this->belongsTo('App\Mobil', 'mobil_id');
    }

    public function users()
    {
        return $this->belongsTo('App\User', 'users_id');
    }

    public function review()
    {
        return $this->hasMany('App\Review');
    }
}
