<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{

    protected $table = 'review';

    protected $fillable = ['review', 'order_id'];

    // public function order()
    // {
    //     return $this->hasOne('App\Order');
    // }
    public function order()
    {
        return $this->belongsTo('App\Order', 'order_id');
    }
}
