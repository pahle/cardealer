<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use App\Mobil;
use App\Status;
use File;
use Illuminate\Support\Facades\DB;

class MobilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

        $mobil = Mobil::all();
        
        return view('mobil.index', compact('mobil'));
        
        }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $status = Status::all();

        return view('mobil.create', compact('status'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'nama' => 'required',
                'desc' => 'required',
                'harga' => 'required',
                'img' => 'required|image|mimes:jpeg,png,jpg',
                'status_id' => 'required',
            ],
            [
                'nama.required' => 'Nama Mobil tidak boleh kosong',
                'desc.required' => 'Deskripsi tidak boleh kosong',
                'harga.required' => 'Harga tidak boleh kosong',
                'img.required' => 'Gambar tidak boleh kosong',
            ]
        );

        $imageName = time() . '.' . $request->img->extension();
        $request->img->move(public_path('images'), $imageName);
        
        $mobil = new Mobil;
        $mobil->nama = $request->nama;
        $mobil->desc = $request->desc;
        $mobil->harga = $request->harga;
        $mobil->img = $imageName;
        $mobil->status_id = $request->status_id;
        $mobil->save();
        Alert::success('Sukses', 'Mobil Baru Berhasil Ditambahkan');

        return redirect('/mobil');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $mobil = Mobil::find($id);

        return view('mobil.show', compact('mobil'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mobil = Mobil::find($id);
        $status = Status::all();

        return view('mobil.edit', compact('mobil', 'status'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate(
            [
                'nama' => 'required',
                'desc' => 'required',
                'harga' => 'required',
                'img' => 'image|mimes:jpeg,png,jpg',
                'status_id' => 'required',
            ],
        );

        $mobil = Mobil::find($id);

        if ($request->has('img')) {
            $path = "images/";
            File::delete($path . $mobil->img);
            $imageName = time() . '.' . $request->img->extension();
            $request->img->move(public_path('images'), $imageName);
            $mobil->img = $imageName;
        }

        $mobil->nama = $request->nama;
        $mobil->desc = $request->desc;
        $mobil->harga = $request->harga;
        $mobil->img = $imageName;
        $mobil->status_id = $request->status_id;
        $mobil->save();
        Alert::success('Sukses', 'Data Mobil Berhasil Diubah');
        return redirect('/mobil');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mobil = Mobil::findorfail($id);
        $mobil->delete();

        $path = "images/";
        File::delete($path . $mobil->img);
        Alert::success('Sukses', 'Data Mobil Berhasil Dihapus');
        return redirect('/mobil');
    }
}