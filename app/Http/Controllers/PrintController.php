<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade\PDF;
use App\Order;
use Auth;

class PrintController extends Controller
{

    function index()
    {
     $order = Order::all();
     return view('pdf')->with('order', $order);
    }

    public function pdf()
    {
        $pdf = \App::make('dompdf.wrapper');
         $pdf->loadHTML($this->convert_customer_data_to_html());
         return $pdf->stream();
    }

    function convert_customer_data_to_html()
    {
        $id = Auth::id();
        $order = Order::where('users_id', $id)->get();
        
     $output = '
     <h3 align="center">Invoice Pembelian Mobil</h3>
     <table width="100%" style="border-collapse: collapse; border: 0px;">
      <tr>
        <th style="border: 1px solid; padding:12px;" width="20%">Pembeli</th>
        <th style="border: 1px solid; padding:12px;" width="30%">Mobil</th>
        <th style="border: 1px solid; padding:12px;" width="15%">Tagihan</th>
      </tr>
     ';  
     foreach($order as $item)
     {
      $output .= '
      <tr>
       <td style="border: 1px solid; padding:12px;">'.$item->users->name.'</td>
       <td style="border: 1px solid; padding:12px;">'.$item->mobil->nama.'</td>
       <td style="border: 1px solid; padding:12px;"> Rp. '.$item->tagihan.'</td>
      </tr>
      ';
      $total = Order::where('users_id', $id)->sum('tagihan');
     }

     
     $output .= '
     <tr>
        <td colspan="2" style="border: 1px solid; padding:12px; text-align: right;">Total</td>
        <td style="border: 1px solid; padding:12px;"> Rp. '. $total .'</td>
      </tr>
     ';

     $output .= '</table>';
     return $output;
    }
}