<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Mobil;
use App\Review;
use App\User;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class ReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $review = Review::all();
        return view('review.index', compact('review'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $id = Auth::id();

        if($id == 2)
        {
            $order = Order::where('users_id', $id)->get();
        }
        else
        {
            $order = Order::all();
        }

        return view('review.create', compact('order'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate(
            [
                'review' => 'required',
                'order_id' => 'required',
            ]
        );

        $review = new Review;

        $review->review = $request->review;
        $review->order_id = $request->order_id;

        $review->save();
        Alert::success('Sukses', 'Review Baru Berhasil Ditambahkan');

        return redirect('/review');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $review = Review::find($id);

        return view('review.show', compact('review'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $review = Review::find($id);
        $order = Order::all();

        return view('review.edit', compact('review', 'order'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate(
            [
                'review' => 'required',
                'order_id' => 'required',
            ]
        );

        $review = Review::find($id);


        $review->review = $request->review;
        $review->order_id = $request->order_id;

        $review->save();
        Alert::success('Sukses', 'Review Baru Berhasil Diubah');

        return redirect('/review');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $review = Review::findorfail($id);
        $review->delete();

        Alert::success('Sukses', 'Data Mobil Berhasil Dihapus');
        return redirect('/review');
    }
}