<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Status;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class StatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $status = Status::all();
        
        return view('status.index', compact('status'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('status.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'status_name' => 'required',
        ],
        [
            'status_name.required' => 'Status Mobil Harus Di Isi (Tidak Boleh Kosong)',
        ]);

        DB::table('status')->insert(
            [   'status_name' => $request['status_name']
            ]
        );
        Alert::success('Sukses', 'Status Mobil Berhasil Ditambahkan');
        return redirect('/status');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $status = Status::find($id);

        return view('status.edit', compact('status'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'status_name' => 'required',
        ],
        [
            'status_name.required' => 'Status Mobil Harus Di Isi (Tidak Boleh Kosong)',
        ]);

        $status = Status::find($id);
 
        $status->status_name = $request->status_name;
        $status->save();
        Alert::success('Sukses', 'Status Mobil Berhasil di Update');
        return redirect('/status');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $status = Status::findorfail($id);
        $status->delete();
        Alert::success('Sukses', 'Data Status Mobil Berhasil Dihapus');
        return redirect('/role');
    }
}
