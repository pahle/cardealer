<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mobil;

class FrontPageController extends Controller
{
    public function home(){

        $mobil = Mobil::all();
        
        return view('frontpage.home', compact('mobil'));
        
        }
}