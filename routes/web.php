<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('frontpage.home');
// });

use Illuminate\Support\Facades\Route;

Route::get('/admin', function () {
    return view('admin');
});

Route::get('/pelanggan', function () {
    return view('pelanggan');
});

Route::group(['middleware' => ['auth']], function () {

    //CRUD Mobil
Route::resource('mobil', 'MobilController');

//CRUD Role
Route::resource('role', 'RoleController');

//CRUD Status
Route::resource('status', 'StatusController');

// CRUD User
Route::resource('user', 'UserController');

});



//AUTH
Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

// CRUD User
Route::resource('user', 'UserController');

// FrontPage
Route::get('/', 'FrontPageController@home');

// Order
Route::resource('order', 'OrderController');

// Review
Route::resource('review', 'ReviewController');

// PDF
Route::get('laporan-pdf', 'PrintController@pdf');